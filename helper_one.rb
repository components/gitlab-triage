# frozen_string_literal: true

require 'gitlab'

module HelperOne
  def one_method(a_name)
    "Hello #{a_name}!"
  end
end
