# frozen_string_literal: true

require_relative 'helper_one'
require_relative 'helper_two'

Gitlab::Triage::Resource::Context.include HelperOne
Gitlab::Triage::Resource::Context.include HelperTwo
